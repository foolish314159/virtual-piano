//
//  Song.swift
//  Virtual Piano
//
//  Created by admin on 26/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

struct Song {
    let path: String
    var song: MIDISong?
    
    init(path: String) {
        self.path = path
        self.song = nil
    }
}