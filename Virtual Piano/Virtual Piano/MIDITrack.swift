//
//  MIDITrack.swift
//  Virtual Piano
//
//  Created by admin on 29/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import AudioToolbox

struct MIDITrack {
    
    var name: String?
    var instrument: String?
    
    /// Start time of the first midi event relative to the whole midi song
    var relativeStartTime: MusicTimeStamp?
    
    /// All note events of the track grouped by timestamp
    var noteEvents = [MIDINoteEvents]()
    
    /// Combine multiple tracks into one single track
    static func combineTracks(tracks: [MIDITrack]) -> MIDITrack {
        var combinedTrack = MIDITrack()
        combinedTrack.relativeStartTime = 0.0
        
        for track in tracks {
            combinedTrack.noteEvents += track.noteEvents
        }
        combinedTrack.noteEvents.sort({ $0.absoluteTime < $1.absoluteTime })
        
        // fix delta times
        var previousTime: Double = combinedTrack.relativeStartTime!
        for i in indices(combinedTrack.noteEvents) {
            combinedTrack.noteEvents[i].deltaTime = combinedTrack.noteEvents[i].absoluteTime - previousTime
            previousTime = combinedTrack.noteEvents[i].absoluteTime
        }
        
        return combinedTrack
    }
}