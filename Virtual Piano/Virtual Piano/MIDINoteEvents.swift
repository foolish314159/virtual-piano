//
//  MIDINoteEvents.swift
//  Virtual Piano
//
//  Created by admin on 29/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import AudioToolbox

struct MIDINoteEvents {
    
    var deltaTime: Double
    var absoluteTime: Double

    var messages: [MIDINoteMessage]
    
    init(delta: Double, absolute: Double) {
        deltaTime = delta
        absoluteTime = absolute
        messages = [MIDINoteMessage]()
    }
    
}