//
//  Instrument.swift
//  Virtual Piano
//
//  Created by admin on 24/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import UIKit

// Values represent program index in a MIDI soundbank
enum Instrument : Int, Printable {
    
    // Piano
    case AcousticGrandPiano = 0
    case BrightAcousticPiano = 1
    //case ElectricGrandPiano = 2
    case HonkyTonkPiano = 3
    case ElectricPianoOne = 4
    case ElectricPianoTwo = 5
    //case Harpsichord = 6
    case Clavinet = 7
    
    // Chromatic Percussion
    case Celesta = 8
    case Glockenspiel = 9
    case MusicBox = 10
    //case Vibraphone = 11
    case Marimba = 12
    case Xylophone = 13
    case TubularBells = 14
    case Dulcimer = 15
    
    // Organ
    //case DrawbarOrgan = 16
    case PercussiveOrgan = 17
    case RockOrgan = 18
    case ChurchOrgan = 19
    case ReedOrgan = 20
    case Accordion = 21
    case Harmonica = 22
    //case TangoAccordion = 23
    
    // Guitar
    case AcousticGuitarNylon = 24
    case AcousticGuitarSteel = 25
    //case ElectricGuitarJazz = 26
    case ElectricGuitarClean = 27
    case ElectricGuitarMuted = 28
    case OverdrivenGuitar = 29
    case DistortionGuitar = 30
    //case GuitarHarmonics = 31
    
    // Bass
    case AcousticBass = 32
    case ElectricBassFinger = 33
    //case ElectricBassPick = 34
    case FretlessBass = 35
    case SlapBassOne = 36
    //case SlapBassTwo = 37
    case SynthBassOne = 38
    //case SynthBassTwo = 39
    
    // Strings
    case Violin = 40
    case Viola = 41
    case Cello = 42
    //case Contrabass = 43
    //case TremoloStrings = 44
    //case PizzicatoStrings = 45
    case OrchestralHarp = 46
    //case Timpani = 47
    
    // Ensemble
    //case StringEnsembleOne = 48
    //case StringEnsembleTwo = 49
    //case SynthStringsOne = 50
    case SynthStringsTwo = 51
    case ChoirAahs = 52
    case VoiceOohs = 53
    //case SynthChoir = 54
    case OrchestraHit = 55
    
    // Brass
    case Trumpet = 56
    case Trombone = 57
    case Tuba = 58
    //case MutedTrumpet = 59
    case FrenchHorn = 60
    case BrassSection = 61
    //case SynthBrassOne = 62
    //case SynthBrassTwo = 63
    
    var description: String {
        switch self {
        case .AcousticGrandPiano:
            return "Acoustic Grand Piano"
        case .BrightAcousticPiano:
            return "Bright Acoustic Piano"
        case .HonkyTonkPiano:
            return "Honky-tonk Piano"
        case .ElectricPianoOne:
            return "Electric Piano 1"
        case .ElectricPianoTwo:
            return "Electric Piano 2"
        case .Clavinet:
            return "Clavinet"
        case .Celesta:
            return "Celesta"
        case .Glockenspiel:
            return "Glockenspiel"
        case .MusicBox:
            return "Music Box"
        case .Marimba:
            return "Marimba"
        case .Xylophone:
            return "Xylophone"
        case .TubularBells:
            return "Tubular Bells"
        case .Dulcimer:
            return "Dulcimer"
        case .PercussiveOrgan:
            return "Percussive Organ"
        case .RockOrgan:
            return "Rock Organ"
        case .ChurchOrgan:
            return "Church Organ"
        case .ReedOrgan:
            return "Reed Organ"
        case .Accordion:
            return "Accordion"
        case .Harmonica:
            return "Harmonica"
        case .AcousticGuitarNylon:
            return "Acoustic Guitar (nylon)"
        case .AcousticGuitarSteel:
            return "Acoustic Guitar (steel)"
        case .ElectricGuitarClean:
            return "Electric Guitar (clean)"
        case .ElectricGuitarMuted:
            return "Electric Guitar (muted)"
        case .OverdrivenGuitar:
            return "Overdriven Guitar"
        case .DistortionGuitar:
            return "Distortion Guitar"
        case .AcousticBass:
            return "Acoustic Bass"
        case .ElectricBassFinger:
            return "Electric Bass (finger)"
        case .FretlessBass:
            return "Fretless Bass"
        case .SlapBassOne:
            return "Slap Bass"
        case .SynthBassOne:
            return "Synth Bass"
        case .Violin:
            return "Violin"
        case .Viola:
            return "Viola"
        case .Cello:
            return "Cello"
        case .OrchestralHarp:
            return "Orchestral Harp"
        case .SynthStringsTwo:
            return "Synth Strings"
        case .ChoirAahs:
            return "Choir Aahs"
        case .VoiceOohs:
            return "Voice Oohs"
        case .OrchestraHit:
            return "Orchestra Hit"
        case .Trumpet:
            return "Trumpet"
        case .Trombone:
            return "Trombone"
        case .Tuba:
            return "Tuba"
        case .FrenchHorn:
            return "French Horn"
        case .BrassSection:
            return "Brass Section"
        default:
            return "Instrument \(self.rawValue)"
        }
    }
    
    static func allValues() -> [Instrument] {
        var allValues = [Instrument]()
        for i in 0...127 {
            if let instrument = Instrument(rawValue: i) {
                allValues.append(instrument)
            }
        }
        return allValues
    }
    
}