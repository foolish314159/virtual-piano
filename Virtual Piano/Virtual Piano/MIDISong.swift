//
//  MIDISong.swift
//  Virtual Piano
//
//  Created by admin on 29/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

class MIDISong {
    
    var title: String?
    var author: String?
    
    var bpm: Double = 0
    
    var tracks: [MIDITrack]
    
    init(title: String, author: String, bpm: Double) {
        self.title = title
        self.author = author
        self.bpm = bpm
        self.tracks = [MIDITrack]()
    }
    
}