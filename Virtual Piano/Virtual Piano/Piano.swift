//
//  Piano.swift
//  Virtual Piano
//
//  Created by admin on 10/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import AVFoundation

/**
    Represents the real piano and defines functions to play notes
*/
class Piano {
    
    // MARK: Constants
    
    private struct SoundbankFormat {
        static let SF2 = "sf2"
        static let DLS = "dls"
    }
    
    struct Soundbank {
        static let GENERAL_USER = "generaluser"
        static let FLUID = "fluid"
        static let ARACHNO = "arachno"
        
        private static func format(instrument: String) -> String {
            switch instrument {
            default: return SoundbankFormat.SF2
            }
        }
    }
    
    private struct MidiController {
        static let VOLUME: UInt8 = 7
        static let SUSTAIN: UInt8 = 64
    }
    
    private static let Notes = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]
    private static let Whites = [0, 2, 4, 5, 7, 9, 11]
    
    // MARK: Initialization
    
    private let engine = AVAudioEngine()
    private let sampler = AVAudioUnitSampler()
    private let melodicBank = UInt8(kAUSampler_DefaultMelodicBankMSB)
    private var soundbankURL: NSURL?
    
    init(soundbank: String) {
        soundbankURL = Piano.urlForSoundbank(soundbank)
        if soundbankURL == nil {
            NSException(name: "Soundbank", reason: "\(soundbank) has no valid URL", userInfo: nil).raise()
        }
        
        engine.attachNode(sampler)
        engine.connect(sampler, to: engine.outputNode, format: nil)
        
        if !sampler.loadSoundBankInstrumentAtURL(soundbankURL, program: UInt8(Instrument.AcousticGrandPiano.rawValue), bankMSB: melodicBank, bankLSB: 0, error: nil) {
            NSException(name: "Soundbank", reason: "Could not load soundbank for \(soundbank)", userInfo: nil).raise()
        }

        var error: NSErrorPointer = nil
        if !engine.startAndReturnError(error) {
            NSException(name: "AudioEngine", reason: "Could not start audio engine: \(error)", userInfo: nil).raise()
        }
    }
    
    // MARK: Playback
    
    /**
    Starts playing a note
    
        :param: midi    Midi number of the note (21 to 108 on a typical 88 key piano)
    */
    func playNote(midi: Int, onChannel: Int) {
        sampler.startNote(UInt8(midi), withVelocity: 64, onChannel: UInt8(onChannel))
    }
    
    /**
    Stops playing a note
    
        :param: midi    Midi number of the note (21 to 108 on a typical 88 key piano)
    */
    func stopNote(midi: Int) {
        sampler.stopNote(UInt8(midi), onChannel: 0)
    }
    
    /**
    Enables/Disables sustain
    
        :param value    Sustain state
    */
    func setSustainEnabled(value: Bool) {
        sampler.sendController(MidiController.SUSTAIN, withValue: value ? 127 : 0, onChannel: 0)
    }
    
    /**
    Changes the volume of the piano
    
        :param volume    0 to 127
    */
    func changeVolume(volume: Int) {
        sampler.sendController(MidiController.VOLUME, withValue: UInt8(volume), onChannel: 0)
    }
    
    /**
    Changes the program(instrument)
    
    :param program    0 to 127
    */
    func changeProgram(program: Int) {
        sampler.loadSoundBankInstrumentAtURL(soundbankURL, program: UInt8(program), bankMSB: melodicBank, bankLSB: 0, error: nil)
    }
    
    // MARK: Utility
    
    /**
    Returns the name for midi number
    
        :param: midi    Midi number of the note (21 to 108 on a typical 88 key piano)
    */
    class func name(midi: Int) -> String {
        let octave = midi / 12 - 1
        let index = midi % 12
        let note = Piano.Notes[index]
        
        return "\(note)\(octave)"
    }
    
    /**
    Returns true if the note is on a white key
    
        :param: midi    Midi number of the note (21 to 108 on a typical 88 key piano)
    */
    func isWhiteKey(midi: Int) -> Bool {
        let index = midi % 12
        return contains(Piano.Whites, index)
    }
    
    /**
    Returns the NSURL of a soundbank if it exists
    */
    static func urlForSoundbank(soundbank: String) -> NSURL? {
        return NSBundle.mainBundle().URLForResource(soundbank, withExtension: Soundbank.format(soundbank))
    }
}