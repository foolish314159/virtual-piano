//
//  PlayAlongController.swift
//  Virtual Piano
//
//  Created by admin on 25/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import UIKit
import AVFoundation

class PlayAlongController: UITableViewController, UISearchBarDelegate {

    // MARK: Model
    
    private var songs = [Int : [Song]]()
    private var recordings = [Song]()
    private var filteredSongs = [Int : [Song]]()
    private var filteredRecordings = [Song]()
    private var folderNames = [Int : String]()
    private var selected: NSIndexPath?

    // MARK: Constants
    
    private struct Storyboard {
        static let SongCellIdentifier = "SongCell"
    }
    
    struct Persistance {
        static let RecordingsDir = "recordings"
        static let SongsDir = "songs"
        static let CustomSongsDir = "\(SongsDir)/custom"
        static let Inbox = "Inbox"
    }
    
    // MARK: Outlets
    
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    @IBOutlet weak var playButton: UIBarButtonItem!
    
    // MARK: Initialization
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.All.rawValue)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    // Copy MIDI files to the document directory and create recording directory on first start
    private func setupDocumentDiretory() {
        let fileManager = NSFileManager()
        if let docs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .AllDomainsMask).first as? NSURL {
            let songsDir = docs.path!.stringByAppendingPathComponent(Persistance.SongsDir)
            if !fileManager.fileExistsAtPath(songsDir) {
                let songsBundleDir = NSBundle.mainBundle().bundlePath.stringByAppendingPathComponent(Persistance.SongsDir)
                fileManager.copyItemAtPath(songsBundleDir, toPath: songsDir, error: nil)
            }
            
            let recsDir = docs.path!.stringByAppendingPathComponent(Persistance.RecordingsDir)
            if !fileManager.fileExistsAtPath(recsDir) {
                fileManager.createDirectoryAtPath(recsDir, withIntermediateDirectories: false, attributes: nil, error: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDocumentDiretory()
        loadFiles()
        loadRecordings()
        
        filteredSongs = songs
        filteredRecordings = recordings
    }
    
    // Load songs from document directory
    private func loadFiles() {
        let fileManager = NSFileManager()
        if let docs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .AllDomainsMask).first as? NSURL {
            let dir = docs.path!.stringByAppendingPathComponent(Persistance.SongsDir)
            if let files = fileManager.contentsOfDirectoryAtPath(dir, error: nil) {
                for file in files {
                    let url = NSURL(fileURLWithPath: dir.stringByAppendingPathComponent(file as! String))
                    appendSongs(url!, fileManager: fileManager)
                }
            }
        }
    }
    
    private func appendSongs(url: NSURL, fileManager: NSFileManager) {
        let index = songs.count
        if fileManager.isDirectory(url) {
            songs[index] = [Song]()
            folderNames[index] = url.lastPathComponent
            let path = url.path!
            let files = fileManager.contentsOfDirectoryAtPath(path, error: nil)
            for file in files! {
                let url = NSURL(fileURLWithPath: path.stringByAppendingPathComponent(file as! String))
                appendSongs(url!, fileManager: fileManager)
            }
        } else if url.pathExtension == "mid" {
            songs[index - 1]!.append(Song(path: url.path!))
        }
    }
    
    // Load recordings from document directory
    private func loadRecordings() {
        let fileManager = NSFileManager()
        if let docs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .AllDomainsMask).first as? NSURL {
            let dir = docs.path!.stringByAppendingPathComponent(Persistance.RecordingsDir)
            if let files = fileManager.contentsOfDirectoryAtPath(dir, error: nil) {
                for file in files {
                    let url = NSURL(fileURLWithPath: dir.stringByAppendingPathComponent(file as! String))
                    recordings.append(Song(path: url!.path!))
                }
            }
        }
    }
    
    // MARK: NavigationBar
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        super.didMoveToParentViewController(parent)
        
        // Back button press
        if parent == nil {
            // TODO
        }
    }
    
    @IBAction func selectSong(sender: UIBarButtonItem) {
        if let pianoVC = self.navigationController?.viewControllers.first as? PianoViewController {
            if let index = selected {
                var song = songForIndexPath(index)
                if song.song == nil {
                    song.song = MIDIParser.parse(song.path)
                    pianoVC.playAlongManager.track = 0
                }
                pianoVC.playAlongModeEnabled = true
                pianoVC.playAlongManager.song = song.song            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: SearchBar

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredSongs = songs
            filteredRecordings = recordings
        } else {
            filterSongs(searchText)
            filterRecordings(searchText)
        }
        
        selected = nil
        playButton.enabled = false
        tableView.reloadData()
    }
    
    private func filterSongs(filter: String) {
        for dir in songs.keys {
            filteredSongs[dir] = songs[dir]!.filter({ (song: Song) -> Bool in
                return song.path.lowercaseString.rangeOfString(filter.lowercaseString) != nil
            })
        }
    }
    
    private func filterRecordings(filter: String) {
        filteredRecordings = recordings.filter({ (song: Song) -> Bool in
            return song.path.lowercaseString.rangeOfString(filter.lowercaseString) != nil
        })
    }

    // MARK: TableView

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return songs.count + 1
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let title = folderNames[section] {
            return "Songs - \(title)"
        }
        return "Recordings"
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < songs.count {
            return filteredSongs[section]!.count
        } else {
            return filteredRecordings.count
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        playButton.enabled = true
        selected = indexPath
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        playButton.enabled = false
        selected = nil
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Storyboard.SongCellIdentifier, forIndexPath: indexPath) as! UITableViewCell

        let title = cell.textLabel!
        let song = songForIndexPath(indexPath)
        title.text = (NSURL(fileURLWithPath: song.path)?.pathComponents?.last as! String).stringByDeletingPathExtension

        return cell
    }
    
    private func songForIndexPath(indexPath: NSIndexPath) -> Song {
        if indexPath.section < songs.count {
            return filteredSongs[indexPath.section]![indexPath.row]
        } else {
            return filteredRecordings[indexPath.row]
        }
    }

}


// Extensions

extension NSFileManager {
    func isDirectory(url: NSURL) -> Bool {
        var isDir: ObjCBool = false
        fileExistsAtPath(url.path!, isDirectory: &isDir)
        return Bool(isDir)
    }
}
