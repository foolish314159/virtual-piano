//
//  PianoNavigationController.swift
//  Virtual Piano
//
//  Created by admin on 25/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import UIKit

class PianoNavigationController: UINavigationController {
   
    override func supportedInterfaceOrientations() -> Int {
        return self.topViewController.supportedInterfaceOrientations()
    }
    
}
