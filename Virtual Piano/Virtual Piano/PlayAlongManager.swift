//
//  PlayAlongManager.swift
//  Virtual Piano
//
//  Created by admin on 31/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import Foundation
import AudioToolbox

protocol PlayAlongManagerDelegate {
    func playAlongManager(shouldPlayNote note: Int, onChannel channel: Int)
    func playAlongManager(shouldStopNote note: Int)
    func playAlongManager(didFinishPlayingDemo finished: Bool)
    
    func playAlongManager(waitForNote note: Int)
    func playAlongManager(didFinishLearnMode finished: Bool)
}

class PlayAlongManager {
    
    // MARK: Constants
    
    private let AllTracks = -1
    
    // MARK: Properties
    
    var delegate: PlayAlongManagerDelegate?
    
    /// Indicate if the manager is currently playing a demo
    var demoPlaying = false {
        didSet {
            if demoPlaying {
                started = false
            }
        }
    }
    /// Indicate if the manager is currently in learn mode
    var started = false {
        didSet {
            if started {
                demoPlaying = false
            }
        }
    }
    
    /// The song associated with play along mode
    var song: MIDISong? {
        didSet {
            if song != nil {
                trackCount = song!.tracks.count
            }
        }
    }
    
    /// The track of the song to use, default is 0
    var track: Int = 0
    var trackCount: Int = 0
    
    func reset() {
        demoPlaying = false
        started = false
        song = nil
    }
    
    // MARK: Learn Mode
    
    private var waitForNotes = NSCondition()
    private var queuedNotes = [Int : Bool]()
    private var waitingForNotes: Bool {
        for played in queuedNotes.values {
            if !played {
                return true
            }
        }
        return false
    }
    
    /// Returns true if learn mode started playing, false otherwise
    func startLearnMode() -> Bool {
        if let midiSong = song {
            if track == AllTracks {
                if song!.tracks.count == trackCount {
                    var combinedTrack = MIDITrack.combineTracks(song!.tracks)
                    song!.tracks.append(combinedTrack)
                }
                return startLearnMode(song!.tracks.count - 1)
            } else {
                return startLearnMode(track)
            }
        }
        
        return false
    }
    
    private func startLearnMode(track: Int) -> Bool {
        if song == nil && track > trackCount {
            return false
        }
        
        started = true
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), {
            var count = 0
            var max = self.song!.tracks[track].noteEvents.count
            var finished = false
            
            // Main loop for playing a track, break here to stop playing
            playLoop: for events in self.song!.tracks[track].noteEvents {
                // Keep track of event count so we can quit after the last one instead of having the thread wait
                count++
                if count >= max {
                    finished = true
                }
                
                self.queuedNotes.removeAll()
                for msg in events.messages {
                    if !self.started {
                        break playLoop
                    }
                    
                    self.queuedNotes[Int(msg.note)] = false
                    dispatch_async(dispatch_get_main_queue(), {
                        self.delegate?.playAlongManager(waitForNote: Int(msg.note))
                    })
                }
                
                // Wait for notes to be played
                self.waitForNotes.lock()
                while (self.waitingForNotes && !finished) {
                    self.waitForNotes.wait()
                }
                self.waitForNotes.unlock()
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.started = false
                self.delegate?.playAlongManager(didFinishLearnMode: true)
            })
        })
        
        return true
    }
    
    func stopLearnMode() {
        started = false
        queuedNotes.removeAll()
        waitForNotes.lock()
        waitForNotes.signal()
        waitForNotes.unlock()
    }
    
    /// Dequeue a note if it's currently queued. Call when a waiting note has been played
    func dequeueNote(note: Int) {
        if queuedNotes[note] != nil {
            queuedNotes[note] = true
            waitForNotes.lock()
            waitForNotes.signal()
            waitForNotes.unlock()
        }
    }
    
    // MARK: Demo
    
    /// Returns true if demo started playing, false otherwise
    func playDemo() -> Bool {
        if let midiSong = song {
            if track == AllTracks {
                if song!.tracks.count == trackCount {
                    var combinedTrack = MIDITrack.combineTracks(song!.tracks)
                    song!.tracks.append(combinedTrack)
                }
                return playDemo(song!.tracks.count - 1, delay: 0.0)
            } else {
                return playDemo(track, delay: 0.0)
            }
        }
        
        return false
    }
    
    private func playDemo(track: Int, delay: MusicTimeStamp) -> Bool {
        if song == nil {
            delegate?.playAlongManager(didFinishPlayingDemo: true)
            return false
        }
        
        demoPlaying = true
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), {
            // Initial delay for a track, should be 0 if only one track is played at a time
            NSThread.sleepForTimeInterval(MIDIParser.beatsToSeconds(delay, forBpm: self.song!.bpm))
            // Main loop for playing a track, break here to stop playing
            if self.song!.tracks.count > track {
                playLoop: for events in self.song!.tracks[track].noteEvents {
                    NSThread.sleepForTimeInterval(MIDIParser.beatsToSeconds(events.deltaTime, forBpm: self.song!.bpm)   )
                    for msg in events.messages {
                        if !self.demoPlaying {
                            break playLoop
                        }
                        dispatch_async(dispatch_get_main_queue(), {
                            self.delegate?.playAlongManager(shouldPlayNote: Int(msg.note), onChannel: Int(msg.channel))
                            NSTimer.scheduledTimerWithTimeInterval(Double(msg.duration), target: self, selector: "stopNote:", userInfo: Int(msg.note), repeats: false)
                        })
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.demoPlaying = false
                self.delegate?.playAlongManager(didFinishPlayingDemo: true)
            })
        })
        
        return true
    }
    
    func stopDemo() {
        demoPlaying = false
    }
    
    @objc private func stopNote(timer: NSTimer) {
        let note = timer.userInfo as! Int
        delegate?.playAlongManager(shouldStopNote: note)
    }
    
}