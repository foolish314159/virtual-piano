//
//  CompositeRect.swift
//  Virtual Piano
//
//  Created by admin on 23/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import UIKit

struct CompositeRect {

    let rectangles: [CGRect]
    
    init(fromRectangles rects: [CGRect]) {
        rectangles = rects
    }
    
    func intersectsRect(rect: CGRect) -> Bool {
        for r in rectangles {
            if CGRectIntersectsRect(r, rect) {
                return true
            }
        }
        return false
    }
    
}
