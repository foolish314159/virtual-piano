//
//  MIDIAudioRecorder.swift
//  Virtual Piano
//
//  Created by admin on 04/06/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import AudioToolbox

class MIDIAudioRecorder {
    
    private var seq: MusicSequence = nil
    private var track: MusicTrack = nil
    private var pressedKeys: Dictionary<Int, CFAbsoluteTime>?
    private var startTime: CFAbsoluteTime = 0
    var isRecording = false
    
    init() {
        reset()
    }
    
    /**
    Reset the previos recording if present
    */
    private func reset() {
        pressedKeys = [Int: CFAbsoluteTime]()
        
        if seq != nil {
            DisposeMusicSequence(seq)
        }
        NewMusicSequence(&seq)
        MusicSequenceNewTrack(seq, &track)
        var tempoTrack: MusicTrack = nil
        MusicSequenceGetTempoTrack(seq, &tempoTrack)
        MusicTrackNewExtendedTempoEvent(tempoTrack, 0, 60)
    }
    
    /**
    Start recording. Should be followed by didPressNote and didReleaseNote calls.
    */
    func start() {
        reset()
        startTime = CFAbsoluteTimeGetCurrent()
        isRecording = true
    }
    
    /**
    Stop recording and return a MusicSequence for the current recording.
    */
    func stop() -> MusicSequence {
        isRecording = false
        return seq
    }
    
    /**
    Call on a key down event
    */
    func didPressNote(note: Int) {
        pressedKeys![note] = CFAbsoluteTimeGetCurrent()
    }
    
    /**
    Call on a key up event
    */
    func didReleaseNote(note: Int) {
        if pressedKeys != nil && pressedKeys![note] != nil {
            let pressed = pressedKeys![note]!
            let delta = CFAbsoluteTimeGetCurrent() - pressed
            let duration = MIDIParser.secondsToBeats(delta, forBpm: 60)
            let timestamp = MIDIParser.secondsToBeats(pressed - startTime, forBpm: 60)
            
            var noteMessage = MIDINoteMessage(channel: 0, note: UInt8(note), velocity: 64, releaseVelocity: 0, duration: Float(duration))
            MusicTrackNewMIDINoteEvent(track, timestamp, &noteMessage)
        }
    }
}
