//
//  UIPianoKey.swift
//  Virtual Piano
//
//  Created by admin on 07/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import UIKit

class UIPianoKey : UIButton {
    
    var keyColor: UIColor?
    var isDown = false
    /// Play Along Manager can force keys to be played, so they don't get overwritten by the user
    var forced = false
    /// Play Along Manager can wait for keys to be played in learn mode. During that time they stay highlighted 
    var waiting = false {
        didSet {
            if waiting {
                highlightColor = UIColor.yellowColor()
                highlighted = true
            } else {
                highlightColor = UIPianoKey.defaultHighlightColor
                highlighted = false
            }
        }
    }
    
    var visibleFrame: CompositeRect?
    var highlightColor: UIColor?
    static let defaultHighlightColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.5)

    private var bgGradient: CGGradientRef {
        var gradient: CGGradientRef
        var colorSpace = CGColorSpaceCreateDeviceRGB()
        let colors = [ backgroundColor!.CGColor, UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5).CGColor ]
        gradient = CGGradientCreateWithColors(colorSpace, colors, gradientLocations)
        return gradient
    }
    
    private var gradientVisible = false
    private var gradientLocations: [CGFloat] {
        return gradientVisible ? [0.0, 0.3] : [0.2, 1.0]
    }
    
    private func drawGradient() {
        let center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)
        let ctx = UIGraphicsGetCurrentContext()
        CGContextDrawRadialGradient(ctx, bgGradient, center, 0, center, self.bounds.size.height, 0)
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        drawGradient()
    }
    
    override var highlighted: Bool {
        get {
            return super.highlighted
        }
        set {
            if newValue {
                backgroundColor = highlightColor
                gradientVisible = true
            } else {
                backgroundColor = keyColor
                gradientVisible = false
            }
        }
    }
    
    func setDisplay() {
        let cornerRadius = bounds.size.width / 40
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: .BottomLeft | .BottomRight, cornerRadii: CGSizeMake(cornerRadius, cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.CGPath
        layer.mask = maskLayer
        
        userInteractionEnabled = false
        highlightColor = UIPianoKey.defaultHighlightColor
        backgroundColor = keyColor
        layer.borderWidth = 1
        layer.borderColor = UIColor.darkGrayColor().CGColor
    }

}

@IBDesignable
class UIPianoWhiteKey: UIPianoKey {

    /**
        Defines the cutout of a white key or which part of the key overlaps with black keys
    */
    enum Cutout {
        case Left, Right, Both
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setDisplay()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setDisplay()
    }
    
    init(frame: CGRect, cutout: Cutout, midi: Int) {
        super.init(frame: frame)
        setDisplay()
        
        let isLeft = cutout == .Left || cutout == .Both
        let isRight = cutout == .Right || cutout == .Both
        
        // Calculate visibleFrame
        // Bottom part is the same for every white key
        let bottomOriginY = frame.origin.y + (frame.size.height * UIPianoBlackKey.PERCENTAGE_HEIGHT)
        let bottomHeight = frame.size.height * (1 - UIPianoBlackKey.PERCENTAGE_HEIGHT)
        let bottomPart = CGRectMake(frame.origin.x, bottomOriginY, frame.size.width, bottomHeight)
        
        // Top part is determined by the cutout
        let blackKeyWidth = frame.size.width * UIPianoBlackKey.PERCENTAGE_WIDTH
        var topOriginX = frame.origin.x
        var topWidth = frame.size.width
        if isLeft {
            topOriginX = frame.origin.x + (0.5 * blackKeyWidth) + UIPianoBlackKey.keyOffset(forMidi: midi - 1, andKeyWidth: blackKeyWidth)
            topWidth -= (0.5 * blackKeyWidth)
        }
        if isRight {
            topWidth -= (0.5 * blackKeyWidth)
            topWidth += UIPianoBlackKey.keyOffset(forMidi: midi + 1, andKeyWidth: blackKeyWidth)
        }
        let topHeight = frame.size.height * UIPianoBlackKey.PERCENTAGE_HEIGHT
        let topPart = CGRectMake(topOriginX, frame.origin.y, topWidth, topHeight)
        
        visibleFrame = CompositeRect(fromRectangles: [bottomPart, topPart])
        
        // Note label for white keys
        let noteLabel = CATextLayer()
        let height: CGFloat = 60
        noteLabel.frame = CGRectMake(bounds.origin.x, bounds.size.height - height, bounds.size.width, height)
        noteLabel.string = Piano.name(midi)
        if UIPianoWhiteKey.font == nil {
            UIPianoWhiteKey.font = CTFontCreateWithName("Helvetica Neue", 16, nil)
        }
        noteLabel.font = UIPianoWhiteKey.font!
        noteLabel.foregroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5).CGColor
        noteLabel.wrapped = false
        noteLabel.alignmentMode = kCAAlignmentCenter
        noteLabel.contentsScale = UIScreen.mainScreen().scale
        self.layer.addSublayer(noteLabel)
    }
    
    private static var font: CTFontRef?
    
    override func setDisplay() {
        keyColor = UIColor.whiteColor()
        super.setDisplay()
    }
   
}

@IBDesignable
class UIPianoBlackKey: UIPianoKey {
    
    static let PERCENTAGE_WIDTH: CGFloat = 0.582
    static let PERCENTAGE_HEIGHT: CGFloat = 0.7
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setDisplay()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setDisplay()
        self.visibleFrame = CompositeRect(fromRectangles: [frame])
    }
    
    /// Offset for black keys, Db and Gb are offset to the left, Eb and Bb to the right
    class func keyOffset(forMidi midi: Int, andKeyWidth width: CGFloat) -> CGFloat {
        let index = midi % 12
        let offsetFactor = width / 5
        
        if index == 1 || index == 6 {
            return -offsetFactor
        } else if index == 3 || index == 10 {
            return offsetFactor
        } else {
            return 0
        }
    }
    
    override func setDisplay() {
        keyColor = UIColor.blackColor()
        super.setDisplay()
    }
}
