//
//  MIDIParser.swift
//  Virtual Piano
//
//  Created by admin on 28/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import Foundation
import AudioToolbox

class MIDIParser {
    
    // MARK: Constants
    
    private static let MIDIType: MusicSequenceFileTypeID = MusicSequenceFileTypeID(kMusicSequenceFile_MIDIType)
    private static let ChannelsToTracksFlag = MusicSequenceLoadFlags(kMusicSequenceLoadSMF_ChannelsToTracks)
    
    private static let FileFlagErase: MusicSequenceFileFlags = MusicSequenceFileFlags(kMusicSequenceFileFlags_EraseFile)
    
    private static let NoTempoInformation: Double = -1
    
    // MARK: Parsing
    
    /**
    Parses a MusicSequence from given path. Returns nil on error
    */
    class func parse(filePath: String) -> MIDISong? {
        var song: MIDISong? = nil
        if let seq = loadSequenceFromFile(filePath) {
            song = parse(seq)
        }
        
        return song
    }
    
    /**
    Parses a MusicSequence. Returns nil on error
    */
    class func parse(seq: MusicSequence) -> MIDISong? {
        var song: MIDISong? = nil
        let bpm = parseTempoTrack(seq)
        let tracks = parseEventTracks(seq)
        song = MIDISong(title: "Test", author: "Author", bpm: bpm)
        song?.tracks += tracks
        DisposeMusicSequence(seq)
        
        return song
    }
    
    /**
    Loads a MusicSequence from given path. Returns nil on error
    */
    class func loadSequenceFromFile(filePath: String) -> MusicSequence? {
        var result: OSStatus
        var seq: MusicSequence = nil
        result = NewMusicSequence(&seq)
        if !checkResult(result) {
            return nil
        }
        
        let url = NSURL(fileURLWithPath: filePath)
        if url == nil {
            return nil
        }
        
        result = MusicSequenceFileLoad(seq, url, MIDIType, ChannelsToTracksFlag)
        if !checkResult(result) {
            return nil
        }
        
        return seq
    }
    
    /**
    Parses the Tempo Track and returns tempo as bpm (beats per minute)
    */
    private class func parseTempoTrack(seq: MusicSequence) -> Double {
        var tempoTrack: MusicTrack = nil
        MusicSequenceGetTempoTrack(seq, &tempoTrack)
        
        var iter: MusicEventIterator = nil
        NewMusicEventIterator(tempoTrack, &iter)
        
        var hasNext: Boolean = false
        var time: MusicTimeStamp = 0
        var eventType: MusicEventType = 0
        var eventData: UnsafePointer<Void> = nil
        var eventDataSize: UInt32 = 0
        MusicEventIteratorHasCurrentEvent(iter, &hasNext)
        while hasNext {
            MusicEventIteratorGetEventInfo(iter, &time, &eventType, &eventData, &eventDataSize)
            
            if Int(eventType) == kMusicEventType_ExtendedTempo {
                let data = UnsafePointer<ExtendedTempoEvent>(eventData).memory
                return Double(data.bpm)
            }
            
            MusicEventIteratorNextEvent(iter)
            MusicEventIteratorHasCurrentEvent(iter, &hasNext)
        }
        
        return NoTempoInformation
    }
    
    /**
    Parses all available MIDITracks and returns them as an array
    */
    private class func parseEventTracks(seq: MusicSequence) -> [MIDITrack] {
        var tracks = [MIDITrack]()
        
        var trackCount: UInt32 = 0
        MusicSequenceGetTrackCount(seq, &trackCount)
        var track: MusicTrack = nil
        for i in 0..<trackCount {
            MusicSequenceGetIndTrack(seq, i, &track)
            
            // for some reason the last note is cut off, so we add another one here
            var size: UInt32 = 0
            var length: MusicTimeStamp = 0
            MusicTrackGetProperty(track, UInt32(kSequenceTrackProperty_TrackLength), &length, &size)
            var noteMessage = MIDINoteMessage(channel: 0, note: 64, velocity: 0, releaseVelocity: 0, duration: 0)
            let timestamp = MIDIParser.secondsToBeats(length + 1.0, forBpm: 60)
            MusicTrackNewMIDINoteEvent(track, timestamp, &noteMessage)
            
            let parsedTrack = parseEventTrack(track)
            if parsedTrack.noteEvents.count > 0 {
                tracks.append(parsedTrack)
            }
        }
        
        return tracks
    }
    
    /**
    Parses a single MusicTrack and returns it as MIDITrack
    */
    private class func parseEventTrack(track: MusicTrack) -> MIDITrack {
        var midiTrack = MIDITrack()
        var noteEvents: MIDINoteEvents? = nil
        
        var iter: MusicEventIterator = nil
        NewMusicEventIterator(track, &iter)
        
        var hasNext: Boolean = false
        var time: MusicTimeStamp = 0
        var previousTime: MusicTimeStamp = 0
        var eventType: MusicEventType = 0
        var eventData: UnsafePointer<Void> = nil
        var eventDataSize: UInt32 = 0
        MusicEventIteratorHasCurrentEvent(iter, &hasNext)
        while hasNext {
            MusicEventIteratorGetEventInfo(iter, &time, &eventType, &eventData, &eventDataSize)
            
            if Int(eventType) == kMusicEventType_MIDINoteMessage {
                let data = UnsafePointer<MIDINoteMessage>(eventData).memory
                // first event
                if noteEvents == nil {
                    midiTrack.relativeStartTime = time
                    noteEvents = MIDINoteEvents(delta: 0.0, absolute: time)
                    previousTime = time
                }

                // group events with same timestamp
                if time > previousTime {
                    midiTrack.noteEvents.append(noteEvents!)
                    noteEvents = MIDINoteEvents(delta: time - previousTime, absolute: time)
                    noteEvents?.messages.append(data)
                } else {
                    noteEvents?.messages.append(data)
                }
            }
            
            MusicEventIteratorNextEvent(iter)
            MusicEventIteratorHasCurrentEvent(iter, &hasNext)
            previousTime = time
        }
        
        return midiTrack
    }
    
    /**
    Saves a MusicSequence to a MIDI file and returns a NSURL to it
    */
    class func saveSequence(seq: MusicSequence, toFilePath path: String) -> NSURL {
        var url = NSURL(fileURLWithPath: path)!
        MusicSequenceFileCreate(seq, url, MIDIType, FileFlagErase, Int16(0))
        return url
    }
    
    private class func checkResult(result: OSStatus) -> Bool {
        return Int(result) == kAudioServicesNoError
    }
    
    class func beatsToSeconds(beats: Double, forBpm bpm: Double) -> Double {
        return beats * 60 / bpm
    }
    
    class func secondsToBeats(seconds: Double, forBpm bpm: Double) -> Double {
        return seconds * bpm / 60
    }
    
}

extension Boolean : BooleanLiteralConvertible {
    public init(booleanLiteral value: Bool) {
        self = value ? 1 : 0
    }
}

extension Boolean : BooleanType {
    public var boolValue : Bool {
        return self != 0
    }
}