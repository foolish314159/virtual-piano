//
//  ViewController.swift
//  Virtual Piano
//
//  Created by admin on 07/05/15.
//  Copyright (c) 2015 com.bierbach.spp. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class PianoViewController: UIViewController, UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, PlayAlongManagerDelegate {
    
    // MARK: Constants
    
    private struct Storyboard {
        static let PlayAlongIdentifier = "PlayAlong"
    }
    
    private struct Images {
        static let PlayImage = UIImage(named: "play2")!
        static let StopImage = UIImage(named: "stop2")!
        static let PreviewImage = UIImage(named: "music")!
        static let SelectImage = UIImage(named: "search")!
        static let CancelImage = UIImage(named: "exit")!
        static let TrackImage = UIImage(named: "list")!
        static let RecordImage = UIImage(named: "record")!
    }
    
    private let VisibleWhiteKeys: CGFloat = 12
    
    // MARK: Model
    
    private let piano = Piano(soundbank: Piano.Soundbank.ARACHNO)
    private let recorder = MIDIAudioRecorder()
    private let recorderPlaybackManager = PlayAlongManager()
    let playAlongManager = PlayAlongManager()
    
    // MARK: Outlets
    
    @IBOutlet weak var keyScrollView: UIScrollView! {
        didSet {
            keyScrollView.delegate = self
        }
    }
    
    @IBOutlet weak var sustainSwitch: UISwitch! {
        didSet {
            sustainSwitch.on = false
            piano.setSustainEnabled(sustainSwitch.on)
        }
    }
    
    @IBOutlet weak var volumeSlider: UISlider! {
        didSet {
            volumeSlider.maximumValue = 1.27
            volumeSlider.value = volumeSlider.maximumValue / 2
            piano.changeVolume(Int(volumeSlider.value * 100))
        }
    }
    
    @IBOutlet weak var instrumentPicker: UIPickerView! {
        didSet {
            instrumentPicker.dataSource = self
            instrumentPicker.delegate = self
        }
    }
    
    @IBOutlet weak var recordButton: UIImageView! {
        didSet {
            recordButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "recordAudio:"))
        }
    }
    
    private var isPlaying = false
    @IBOutlet weak var playbackButton: UIImageView! {
        didSet {
            playbackButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "togglePlayback:"))
        }
    }
    
    // MARK: Initialization
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Landscape.rawValue)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let orientation = UIDevice.currentDevice().orientation
        if orientation != .LandscapeLeft && orientation != .LandscapeRight {
            UIDevice.currentDevice().setValue(UIInterfaceOrientation.LandscapeLeft.rawValue, forKey: "orientation")
        }
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        openMidiFile()
        // When opened from background viewDidAppear is called before AppDelegate's openURL, so wait for notification instead
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openMidiFile", name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    @objc
    private func openMidiFile() {
        // App opened with a MIDI file
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let file = appDelegate.openedFile {
            appDelegate.openedFile = nil
            playAlongManager.track = 0
            playAlongModeEnabled = true
            playAlongManager.song = MIDIParser.parse(file.path!)
            
            // Copy to custom songs directory
            let fileManager = NSFileManager()
            if let docs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .AllDomainsMask).first as? NSURL {
                let customSongsDir = docs.path!.stringByAppendingPathComponent(PlayAlongController.Persistance.CustomSongsDir)
                let path = customSongsDir.stringByAppendingPathComponent(file.lastPathComponent!)
                if !fileManager.fileExistsAtPath(path) {
                    fileManager.copyItemAtPath(file.path!, toPath: path, error: nil)
                }
                fileManager.removeItemAtPath(file.path!, error: nil)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        playAlongManager.delegate = self
        recorderPlaybackManager.delegate = self
        
        if pianoKeys.count <= 0 {
            loadKeysFrom(21, to: 108)
        }
    }
    
    // Keep track of all keys for touch handling
    var pianoKeys = [UIPianoKey]()
    
    private func loadKeysFrom(start: Int, to stop: Int) {
        let top: CGFloat = 0
        let keyHeightWhite = keyScrollView.frame.size.height
        let keyHeightBlack = UIPianoBlackKey.PERCENTAGE_HEIGHT * keyHeightWhite
        let keyWidthWhite = keyScrollView.frame.size.width / VisibleWhiteKeys
        let keyWidthBlack = UIPianoBlackKey.PERCENTAGE_WIDTH * keyWidthWhite
        
        let keyCount = stop - start
        
        // White keys first
        // Keep track of x position of latest white key
        var x = 0
        for i in 0...keyCount {
            let midi = i + start
            
            if piano.isWhiteKey(midi) {
                let key = UIPianoWhiteKey(frame: CGRectMake(CGFloat(x) * keyWidthWhite, top, keyWidthWhite, keyHeightWhite), cutout: cutout(forMidi: midi), midi: midi)
                key.tag = midi
                pianoKeys.append(key)
                keyScrollView.addSubview(key)
                x++
            }
        }
        
        // Black keys second because they need to be drawn on top
        x = 0
        for i in 0...keyCount {
            let midi = i + start
            
            if piano.isWhiteKey(midi) {
                x++
            } else {
                let originX = (CGFloat(x) * keyWidthWhite) - (0.5 * keyWidthBlack) + UIPianoBlackKey.keyOffset(forMidi: midi, andKeyWidth: keyWidthBlack)
                let key = UIPianoBlackKey(frame: CGRectMake(originX, top, keyWidthBlack, keyHeightBlack))
                key.tag = midi
                pianoKeys.append(key)
                keyScrollView.addSubview(key)
            }
        }
        
        keyScrollView.contentSize = CGSizeMake(CGFloat(x) * keyWidthWhite, keyHeightWhite)
        keyScrollView.scrollEnabled = false
        keyScrollView.setContentOffset(CGPointMake(CGFloat(x-6) * keyWidthWhite / 2, keyScrollView.contentOffset.y), animated: true)
    }
    
    // Get cutout for midi value, only makes sense for white keys
    private func cutout(forMidi midi: Int) -> UIPianoWhiteKey.Cutout {
        let isLeft = !piano.isWhiteKey(midi - 1)
        let isRight = !piano.isWhiteKey(midi + 1)
        
        if isLeft && isRight {
            return .Both
        } else if isLeft {
            return .Left
        } else {
            return .Right
        }
    }
    
    // MARK: PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Instrument.allValues().count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return Instrument.allValues()[row].description
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        piano.changeProgram(Instrument.allValues()[row].rawValue)
    }
    
    // MARK: Gestures
    
    private var moveTimer: NSTimer?
    
    private var moveOffset: CGFloat {
        return self.view.bounds.width / 60
    }
    @IBAction func moveLeft(sender: UIButton) {
        if moveTimer == nil {
            moveTimer = NSTimer.scheduledTimerWithTimeInterval(0.03, target: self, selector: "move:", userInfo: -moveOffset, repeats: true)
        }
    }
    
    @IBAction func moveRight(sender: UIButton) {
        if moveTimer == nil {
            moveTimer = NSTimer.scheduledTimerWithTimeInterval(0.03, target: self, selector: "move:", userInfo: moveOffset, repeats: true)
        }
    }
    
    // Keep moving keyboard left/right while finger is down
    func move(timer: NSTimer) {
        if let offset = timer.userInfo as? CGFloat {
            var currentOffset = keyScrollView.contentOffset
            currentOffset.x += offset
            if currentOffset.x <= 0 || currentOffset.x >= keyScrollView.contentSize.width - keyScrollView.frame.size.width {
                moveTimer?.invalidate()
                moveTimer = nil
            } else {
                keyScrollView.setContentOffset(currentOffset, animated: false)
            }
        }
    }
    
    @IBAction func stopMove(sender: UIButton) {
        moveTimer?.invalidate()
        moveTimer = nil
    }
    
    func onKeyDown(sender: UIButton!, withHighlightColor color: UIColor, onChannel channel: Int = 0) {
        let key = sender as! UIPianoKey
        key.highlightColor = color
        key.forced = true
        onKeyDown(sender, onChannel: channel)
    }
    
    func onKeyDown(sender: UIButton!, onChannel channel: Int = 0) {
        let key = sender as! UIPianoKey
        key.isDown = true
        key.highlighted = true
        piano.playNote(key.tag, onChannel: channel)
        
        if key.waiting {
            key.waiting = false
            playAlongManager.dequeueNote(key.tag)
        }
        
        if recorder.isRecording {
            recorder.didPressNote(key.tag)
        }
    }
    
    func onKeyUp(sender: UIButton!) {
        let key = sender as! UIPianoKey
        key.isDown = false
        key.highlighted = false
        piano.stopNote(key.tag)
        key.highlightColor = UIPianoKey.defaultHighlightColor
        key.forced = false
        
        if key.waiting {
            // set again to force highlight
            key.waiting = true
        }
        
        if recorder.isRecording {
            recorder.didReleaseNote(key.tag)
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in touches {
            touchDown(event.allTouches() as! Set<UITouch>)
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in touches {
            touchDown(event.allTouches() as! Set<UITouch>)
        }
    }
    
    private func touchDown(touches: Set<UITouch>) {
        // Keep track of all positions in case of a multi touch
        var pos = [CGPoint]()
        for touch in touches {
            pos.append(touch.locationInView(keyScrollView))
        }
        
        for key in pianoKeys {
            checkKey(key, pos: pos)
        }
    }
    
    private func checkKey(key: UIPianoKey, pos: [CGPoint]) {
        // For multitouch we have to keep track of all hit positions so the notes aren't played multiple times while moving the finger over the same key
        var containsPoint = false
        for p in pos {
            // Treat hit as small rect because a finger isn't 1 pixel small
            // TODO: size of finger
            let fingerSize: CGFloat = 1.0
            let finger = CGRectMake(p.x - fingerSize / 2, p.y - fingerSize / 2, fingerSize, fingerSize)
            if key.visibleFrame!.intersectsRect(finger) {
                containsPoint = true
            }
        }
        
        if containsPoint && !key.isDown {
            onKeyDown(key)
        } else if containsPoint {
            // Only play a note once while moving over the same key
        } else if key.isDown && !containsPoint && !key.forced {
            onKeyUp(key)
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        for key in pianoKeys {
            if key.isDown && !key.forced {
                onKeyUp(key)
            }
        }
    }
    
    override func touchesCancelled(touches: Set<NSObject>, withEvent event: UIEvent) {
        for key in pianoKeys {
            if key.isDown  && !key.forced {
                onKeyUp(key)
            }
        }
    }
    
    @IBAction func toggleSustain(sender: UISwitch) {
        piano.setSustainEnabled(sender.on)
    }
    
    @IBAction func changeVolume(sender: UISlider) {
        piano.changeVolume(Int(sender.value * 100))
    }
    
    func recordAudio(gesture: UITapGestureRecognizer) {
        if gesture.state == .Ended {
            if recorder.isRecording {
                let fileManager = NSFileManager()
                if let docs = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .AllDomainsMask).first as? NSURL {
                    let seq = recorder.stop()
                    let format = NSDateFormatter()
                    format.dateFormat = "yyyy-MM-dd_HH-mm"
                    let now = format.stringFromDate(NSDate())
                    let filePath = "\(PlayAlongController.Persistance.RecordingsDir)/\(now).mid"
                    MIDIParser.saveSequence(seq, toFilePath: docs.path!.stringByAppendingPathComponent(filePath))
                    recorderPlaybackManager.song = MIDIParser.parse(seq)
                }
                recordButton.image = Images.RecordImage
            } else {
                recorder.start()
                recordButton.image = Images.StopImage
            }
            println("Recording: \(recorder.isRecording)")
        }
    }
    
    func togglePlayback(gesture: UITapGestureRecognizer) {
        switch gesture.state {
        case .Ended:
            isPlaying = !isPlaying
            updatePlaybackViews()
            if isPlaying {
                recorderPlaybackManager.playDemo()
            } else {
                recorderPlaybackManager.stopDemo()
            }
        default:
            println("default")
        }
    }
    
    private func updatePlaybackViews() {
        if isPlaying {
            playbackButton.image = Images.StopImage
        } else {
            playbackButton.image = Images.PlayImage
        }
    }
    
    // MARK: Play Along Mode
    
    private var SelectImage: UIImage { return playAlongModeEnabled ? Images.CancelImage : Images.SelectImage }
    private var DemoImage: UIImage { return playAlongManager.demoPlaying ? Images.StopImage : Images.PreviewImage }
    private var StartImage: UIImage { return playAlongManager.started ? Images.StopImage : Images.PlayImage }
    
    var playAlongModeEnabled = false {
        didSet {
            updateViews()
        }
    }
    
    private func updateViews() {
        playAlongSelectTrackButton.hidden = !playAlongModeEnabled
        playAlongSelectTrackButton.enabled = playAlongModeEnabled
        playAlongStartButton.hidden = !playAlongModeEnabled
        playAlongStartButton.enabled = playAlongModeEnabled
        playAlongDemoButton.hidden = !playAlongModeEnabled
        playAlongDemoButton.enabled = playAlongModeEnabled
        
        playAlongStartButton.setImage(StartImage, forState: .Normal)
        playAlongDemoButton.setImage(DemoImage, forState: .Normal)
        playAlongSelectButton.setImage(SelectImage, forState: .Normal)
    }
    
    @IBOutlet weak var playAlongSelectTrackButton: UIButton!
    @IBOutlet weak var playAlongStartButton: UIButton!
    @IBOutlet weak var playAlongDemoButton: UIButton!
    @IBOutlet weak var playAlongSelectButton: UIButton!
    
    @IBAction func playAlongSelectTrack(sender: UIButton) {
        let alert = UIAlertController(title: "Select a track", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        for i in -1..<playAlongManager.trackCount {
            let trackTitle = (i == -1 ? "All Tracks" : "Track \(i)")
            let action = UIAlertAction(title: trackTitle, style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) -> Void in
                self.playAlongManager.track = i
            })
            alert.addAction(action)
        }
        alert.popoverPresentationController?.sourceView = playAlongSelectTrackButton
        alert.popoverPresentationController?.sourceRect = playAlongSelectTrackButton.bounds
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func playAlongStart(sender: UIButton) {
        if playAlongManager.started {
            for key in pianoKeys {
                if key.waiting {
                    key.waiting = false
                }
            }
            playAlongManager.stopLearnMode()
        } else {
            playAlongManager.startLearnMode()
        }
        updateViews()
    }
    
    @IBAction func playAlongDemo(sender: UIButton) {
        if playAlongManager.demoPlaying {
            playAlongManager.stopDemo()
        } else {
            playAlongManager.playDemo()
        }
        updateViews()
    }
    
    @IBAction func playAlongSelect(sender: UIButton) {
        if playAlongModeEnabled {
            playAlongModeEnabled = false
            playAlongManager.reset()
        } else {
            performSegueWithIdentifier(Storyboard.PlayAlongIdentifier, sender: sender)
        }
    }
    
    func playAlongManager(shouldPlayNote note: Int, onChannel channel: Int) {
        for key in pianoKeys {
            if key.tag == note {
                onKeyDown(key, withHighlightColor: keyColorForChannel(channel), onChannel: channel)
                break
            }
        }
    }
    
    func playAlongManager(shouldStopNote note: Int) {
        for key in pianoKeys {
            if key.tag == note {
                onKeyUp(key)
                break
            }
        }
    }
    
    func playAlongManager(didFinishPlayingDemo finished: Bool) {
        updateViews()
        
        isPlaying = false
        updatePlaybackViews()
    }
    
    func playAlongManager(waitForNote note: Int) {
        for key in pianoKeys {
            if key.tag == note {
                key.waiting = true
                break
            }
        }
    }
    
    func playAlongManager(didFinishLearnMode finished: Bool) {
        updateViews()
    }
    
    private var channelColors = [Int : UIColor]()
    private func keyColorForChannel(channel: Int) -> UIColor {
        if let color = channelColors[channel] {
            return color
        } else {
            let color = UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 0.5)
            channelColors[channel] = color
            return color
        }
    }
    
}



// MARK: Extensions

// Forward touch events from a UIScrollView to the subviews
extension UIScrollView {
    public override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.nextResponder()?.nextResponder()?.touchesBegan(touches, withEvent: event)
    }
    public override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.nextResponder()?.nextResponder()?.touchesMoved(touches, withEvent: event)
    }
    public override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.nextResponder()?.nextResponder()?.touchesEnded(touches, withEvent: event)
    }
    public override func touchesCancelled(touches: Set<NSObject>!, withEvent event: UIEvent!) {
        self.nextResponder()?.nextResponder()?.touchesCancelled(touches, withEvent: event)
    }
}
